data class Familiar(val name:String = "Rodrigo", val age:Int = 24, val colorCabello:String = "cafe")


data class Result(val result:Int, val status:Boolean)

fun calcular(a:Int, b:Int): Result{
    if(a*2 > b){
        return Result(a * 2, true)
    }else{
        return Result(a, false)
    }
}

fun main(args: Array<String>) {
    /*val rodrigo = Familiar()

    val name = rodrigo.name
    val age = rodrigo.age
    val color = rodrigo.colorCabello*/

    //val (name, age, color) = Familiar()
    val (name, _, color) = Familiar()  //en caso de no querer un dato

    println(name)
    println(color)

    //hacer la desconstruccion de una funcion
    val (resultado, status) = calcular(4, 3)
    println(resultado)
    println(status)

}