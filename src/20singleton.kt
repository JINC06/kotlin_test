
//patron singleton solo se necesita generar un object

object Validaciones {

    //funcion en una sola linea
    fun passwordValid(psw:String) = psw.isNotEmpty() && psw.length > 10

    fun esNumero(dato:Any) = dato is Int

}


//ahora a partir de una clase

class ClaseUniversal {

    companion object {
        fun create():ClaseUniversal = ClaseUniversal()
    }

}


fun main(args: Array<String>) {

    println("Ingresa tu contraseña: ")
    val passw:String = readLine()?: ""        //!! evitar esta puntuacion
    println(Validaciones.passwordValid(passw))

    println("Ingresa un numero o no: ")
    try {
        val numero: Any = readLine()?.toInt() ?: ""   //else if operator
        println(numero)
        println(Validaciones.esNumero(numero))
    }catch (e:Exception){
        println(e)
    }


    ///////////////
    //solo genera una instancia del mismo objecto
    var claseUniversalEnMain = ClaseUniversal.create()

}


fun data(){
    //solo genera una instancia del mismo objecto
    var claseUniversalData = ClaseUniversal.create()
}