
//poner un valor por defecto
fun saluda(saludo: String = "Hola a todos"){
    println(saludo)
}

fun otraFuncion (edad: Int = 0, esAdulto: Boolean = false, tieneRopa: Boolean = true){

}

data class P1(val a: Int = 2)

fun main(args: Array<String>) {
    saluda("Hola everybody")
    saluda()

    //asignar valores en la funciones
    otraFuncion(esAdulto = true, edad = 30, tieneRopa =  true)
    otraFuncion(esAdulto = true, tieneRopa =  true)
    otraFuncion()
    otraFuncion(32, true, true)

    otraFuncion(30, esAdulto = true)
    //otraFuncion(edad = 30, true) //este no se puede

    val objetoO = P1(3)

}