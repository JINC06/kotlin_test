fun main(args: Array<String>) {

    var a:String = "abc"
    //a = null --error

    var b:String?
    b = null

    val lA = a.length
    val lB1 = b?.length   //llamada segura  ?    si no es nula le optiene el length si es nulla corta la operacion y regresa null por eso aqui es Int?
    val lB2 = b?.length ?: 0   //if operator siempre tener un dato


    //? este operador es lo mismo que lo siguiente
    val lC = if(b != null){
        b.length
    }else{
        0
    }

    println(lA)
    println(lB1)
    println(lB2)


    val persona:Persona2? = Persona2("Sebas", 1, "Cafes")
    val name = persona?.name


    if(persona != null){
        //Salte
    }

    persona?.let {
        //llamada segura todo lo que se ejecute aqui es siempre y cuando persona sea distinto a null
        //persona inicializada
    }

    persona.let {
        //null
    }

}