fun main(args: Array<String>) {
    val listaNumeros = intArrayOf(1,2,3,4,5,6,7,8,9)

    for (i in listaNumeros.indices) {   //traer los indices
        println("${listaNumeros[i]}")
    }

    val frutas = listOf("manzana", "piña", "uva")
    for (fruta in frutas){  //cada uno de los valores
        println(fruta)
    }

    var x = 3
    while( x > 0 ){
        x--
        println(x)
    }

    var sum:Int = 0
    var input:String?  = ""

    do{
        print("Ingresa un numero: ")
        input = readLine()
        sum += input?.toInt() ?: 0
    }while (input != "0")

    println("La suma es: $sum")

}

