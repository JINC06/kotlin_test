class Ejemplo {
    fun hola(){
        println("Hola Kotlin")
    }
}

//Es una manera de generar un constructor
/*class Persona constructor(val nombre:String) {

}*/

class Persona (val nombre:String) {
    init {
        println("Nombre $nombre")
    }

    constructor(name:String, edad:Int) :this(name){
        println("Nombre $nombre, edad $edad")
    }
}

//open es para hacerlo padre o poderlo hacer herencia
open class Base(val inicial:Int){
    //open para permitir la sobre escritura
    open fun imprimerAlgo(){
        println("Algo $inicial")
    }
}

//extender o herencia entre clases
class General(val d:Int) : Base(d){
    override fun imprimerAlgo() {
        super.imprimerAlgo()
        val suma = 4 + d
    }
}

fun main(args: Array<String>){

    val persona = Persona("Julio Nava")
    val persona2 = Persona("Jose Alvarez", 21)
    println(persona.toString())


    val general = General(4)
    general.imprimerAlgo()
}