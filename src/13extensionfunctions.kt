fun main(args: Array<String>) {

    "Hola mi nombre es Sebastian".print()

    println(4.multiply(2))
    println(4 multiply 5) //para este sirve el infix

    val listaNombres = mutableListOf("Julio", "Nava", "Cordero")
    listaNombres.swap(0, 1)
    println(listaNombres.toString())
}


fun <T> MutableList<T>.swap(index1:Int, index2:Int){
    val temp = this[index1]
    this[index1] = this[index2]
    this[index2] = temp
}

fun String.print() {
    println(this)
}

/*fun Int.multiply(numM: Int): Int 44{
    return this * numM
}*/

infix fun Int.multiply(numM: Int): Int {
    return this * numM
}

//infix

//Las Funciones de Extensión son un aporte grandioso para el desarrollo de aplicaciones. ¿Por qué?
//Nos permite agregarle métodos a una clase sin la necesidad de extenderlas. Y más aún cuando una clase es final como
// es muy común encontrarse en el SDK de android.