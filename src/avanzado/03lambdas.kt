package avanzado


data class Auto(val color:String, val llantas:Int, val puertas:Int)

fun main(args: Array<String>) {

    val num:Int? = null

    val resultado = sum(4,7)
    println(resultado)

    val resultadoMinCuatro = minCuatro("Julio")
    println(resultadoMinCuatro)


    //?   num != null
//    num?.let {
//
//    }

    val miAuto:Auto? = Auto("rojo", 4, 4)

    //aqui podemos usar los miembros del dato
    miAuto?.let {
        it.puertas
        it.color
        it.llantas

        with(it){
            color
            puertas
            llantas
        }
    }

    //otro lambda
//    with(miAuto){
//        color
//    }

}

//crear una funcion apartir de una variable
val sum: (Int, Int) -> Int = { x, y -> x + y }

//it hace referencia al string
val minCuatro: (String) -> Boolean = { it.length > 4 }

