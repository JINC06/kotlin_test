package avanzado

fun esNumero(dato: Any?) = dato is Int || dato is Int?

fun fail(mensaje:String): Nothing {
    throw IllegalArgumentException(mensaje)
}

fun main(args: Array<String>) {

    //2da parte
//    println("Ingresa cualquier cosa: ")
//
//
//    val num:Any? = try{
//        readLine()?.toInt()
//    }catch (e:NumberFormatException){
//        "No Int"
//    }
//
//    println("Variable: ")
//    println(num)
//    println("Función: ")
//    println(esNumero(num))

    var name = ""
    val persona:PersonaKT? = null
    try{
        //? es la condicion
        name = persona?.nombre ?: fail("Nombre requerido")
    }catch (e:IllegalArgumentException){
        println(e)
        println(name)
    }


    //1ra parte
//    println("Ingresa cualquier cosa: ")
//    var numero:Any? = null
//    try{
//
//        numero = readLine()?.toInt()
//        println(numero)
//        println(esNumero(numero))
//
//    }catch (e: NumberFormatException){
//        println(e)
//        println(numero)
//        println(esNumero(numero))
//    }

}