package avanzado

//inline function

//inline fun String.filter(predicate: (Char) -> Boolean): String {
//    return filterTo(StringBuilder(), predicate).toString()
//}

//2paso le quito el inline
fun String.filter(predicate: (Char) -> Boolean): String {
    return filterTo(StringBuilder(), predicate).toString()
}

// Tools -> Kotlin -> Show Kotlin Bytecode

fun main(args: Array<String>) {
    //esta funcion en linea se agrega a los strings y la mandas a llamar a it le asigna la H cuando es true la condicion

    val a = "Hola".filter {
        it == 'H'
    }

    print(a)
}