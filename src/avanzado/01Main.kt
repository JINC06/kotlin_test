package avanzado

fun main(args: Array<String>) {

    val persona = PersonaKT("Julio", 26)
    val (nombre, edad) = persona
    println(nombre)
    println("${edad * 2}")

}


data class PersonaKT(val nombre:String, val edad: Int)