package avanzado

//es muy parecido a los enumeradores

//1ra parte
//sealed class Intencion{
//    class Refrescar:Intencion()
//    class RecargarMas:Intencion()
//}

sealed class Intencion {
    object Refrescar:Intencion(){
        override fun log() {
            println("Refresh")
        }
    }

    object RecargarMas:Intencion(){
        override fun log() {
            println("Load More Data")
        }

    }

    data class Error(val razon:String):Intencion(){
        override fun log() {
            println("$razon")
        }

    }

    //como es funcion abstracta tiene que heredar
    abstract  fun log();
}

fun main(args: Array<String>) {

//1ra parte
//    val intencion:Intencion = Intencion.RecargarMas()
//
//    val output = when(intencion){
//        is Intencion.Refrescar -> "refresh"
//        is Intencion.RecargarMas -> "load more data"
//    }
//
//    println(output)


    //val intencion:Intencion = Intencion.Error("Super error")
    val intencion:Intencion = Intencion.RecargarMas
    val output = when(intencion){
        is Intencion.RecargarMas -> "Carga mas datos"
        is Intencion.Refrescar -> "Refrescar"
        is Intencion.Error -> "Error"
    }


    intencion.log()
    println(output)

}