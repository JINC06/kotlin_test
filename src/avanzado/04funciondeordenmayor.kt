package avanzado

//High order function

fun main(args: Array<String>) {


    suma(6, 8) {    //aqui es un compartamiento especial estos brakes representan la funcion como parametro
        6 + 9
        if("".isBlank()){
            println("True")
        }
        println("Funcion de orden mayor")
    }

    "Hola".funcion {
        println(it)
    }

    //Esta funcion tambien es una funcion extendida de generico
    with("hola"){
        print(length)
    }
}

//aqui estamos haciendo que la funcion de orden mayor reciba un string y se pase asi mismo
//La clase string hereda este metodo extiendo su funcionalidad
fun String.funcion(func:(String) -> Unit){
    //despedazar el string
    func(this)
    println(this.length)
}


//Unit que no regrese nada lambdas funciones anonimas
fun suma(num:Int, num2:Int, func:() -> Unit) {
    func()
    println("Suma de $num y $num2 = ${num + num2}")
    //func()
}
