import kotlin.math.tan

fun main(args: Array<String>) {
    //when es como switch pero inteligente

    type(8)
    type("Hola")

    val mabel = Persona2("Mabel", 40, "Cafes")
    val unknow = Persona2("killer", 40, "Cafes")

    println(whoIs(mabel))
    println(whoIs(unknow))


    rango(5)
    rango(50)
    rango(500)
}


fun type(dato: Any) {
    when (dato) {
        is String -> {   //si son muchas lineas
            println(dato.length)
        }
        is Int -> println("${dato * 2}")
        is Boolean -> {
            println("Es un booleano")
            println(dato)
        }
        is Persona2 -> println("${dato.name} ${dato.age}")
        else -> println("No se que es objeto es")
    }
}
//
//fun whoIs(persona: Persona2){
//    when(persona.name){
//        "Mabel" -> "Puedes entrar"
//        "Darla" -> "Puedes entrar"
//        "Raul" -> "Puedes entrar"
//        else -> "Corre de la casa"
//    }
//}


//fun whoIs(persona: Persona2) =
//        when (persona.name) {
//            "Mabel" -> "Puedes entrar"
//            "Darla" -> "Puedes entrar"
//            "Raul" -> "Puedes entrar"
//            else -> "Corre de la casa"
//        }


//resumiendo funcion anterior
fun whoIs(persona: Persona2) =
        when (persona.name) {
            "Mabel", "Darla", "Raul" -> "Puedes entrar"
            else -> "Corre de la casa"
        }

fun rango(numero:Int){
    when(numero){
        in 1..10 -> println("Este es un numero positivo entre 1 y 10")
        in 10..100 -> println("Este es un numero positivo entre 10 y 100")
        else -> println("Este numero esta fuera de los rangos definidos")
    }
}