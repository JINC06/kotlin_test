//Declarar funciones todas la funciones regresa un Unit por defecto Unit = void
fun hello():Unit{
    println("Hola a todos")
}


//fun suma(val a:Int, val b:Int):Int{  <val esto era antes
fun suma(a:Int, b:Int):Int{
    return a + b
}

//recibe parametros pero no regresa nada
fun suma2(a:Int, b:Int){
    println("la suma de $a + $b es igual a ${a+b}")
}

/*fun max(a:Int, b:Int): Int {
    if (a > b)
        return a
    else
        return b
}*/

//este es la version corta de la de arriba
fun max(a:Int, b:Int) = if (a>b) a else b


fun main(args: Array<String>){
    println(max(4, 9))
}