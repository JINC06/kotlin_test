fun main(args: Array<String>) {

    val a = esMayor(6)
    defineTipo("Heyy!!")
    defineTipo(10)
    defineTipo(Persona("Julio Nava"))
}

//Esta es la manera comun se puede resumir como el siguiente
//fun esMayor(data: Int): Boolean{
//    if(data > 0){
//        println("$data")
//        return true
//    }else{
//        println("$data")
//        return false
//    }
//}


fun esMayor(data: Int): Boolean =
    if(data > 0){
        println("$data")
        true
    }else{
        println("$data")
        false
    }

//fun esMayorShort(data:Int): Boolean = if (data > 0) true else false
//fun esMayorShort(data:Int): Boolean = data > 0

//Any es cualquier cosa es el Object
// el if es mas inteligente
fun defineTipo(data: Any) {
    if(data is String) {
        println(data.length)   //no se tiene que hacer el casteo   smartcast
    } else if(data is Int){
        println("${data * 2}")
    } else if(data is Persona){
        println(data.nombre)
    }
}