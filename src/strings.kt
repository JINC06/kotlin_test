fun main(args: Array<String>) {

    // String y string templates

    val srt: String = "Julio Nava"


    val nombre = "Mabel"
    val edad = 25

    val sentencia:String = "Ella es " + nombre + " y tiene " + edad + " años"
    val sentencia2  = "Ella es $nombre y tiene $edad años"

    println(sentencia)
    println(sentencia2)


    val cumple = "$nombre cumple ${edad + 1}"
    println(cumple)

    //con tres comillas puedes agregar formato y alinearlo a la izquierda
    val text = """
        Este es un string que quiere imprimir
        que es demasiado largo y tiene
        varios renglones
    """

    println(text)

    //Esto es para alinear el texto
    val text2:String = """
        |Este es un string que quiere imprimir
        |que es demasiado largo y tiene
        |varios renglones
    """.trimMargin()

    println(text2)

    //puedes cambiar el formato
    val text3:String = """
        >Este es un string que quiere imprimir
        >que es demasiado largo y tiene
        >varios renglones
    """.trimMargin(">")

    println(text3)
}