fun main(args: Array<String>) {

    val arrA = intArrayOf(1,232,12,23,4)
    val listA = arrayListOf<String>("Hola", "Mundo", "Java")
    val listB = arrayListOf("Hola", "Mundo", "Java")

    //obtener
    val num =  arrA[0] //arrA.get(0)

    //poner
    arrA.set(2, 10)
    arrA[2] = 10

    //los arreglos son immutables
    val mutableList = mutableListOf(1,2,3,4,5,6,7,8,9)
    mutableList.add(10)
    mutableList.add(2, 10)
    mutableList[0] = 2

    //Lista de clave valor
    val map = mutableMapOf<String, Int>(Pair("Key", 10))
    map.put("Key2", 30)

}